const os = require('os');
const path = require('path');
const workerpool = require('workerpool');
const logger = require('./logger')('index.js');
const IO = require('./service/IO');
const db = require('./db');
const statesDict = require('./service/states_us');
const ParserCategories = require('./service/ParserCategories');

const STAGE_ONE = 1;
const STAGE_TWO = 2;
const STAGE_THREE = 3;

const pool = workerpool.pool(path.resolve(__dirname, 'service/worker.js'), {
  workerType: 'process',
});
const cpuCount = os.cpus().length - 1;

logger.info('Script is started. Using %d cpu cores.', cpuCount);

const getCategoriesWithStates = categories => {
  let result = [];
  const allStates = Object.keys(statesDict);
  categories.forEach(category => {
    result = result.concat(
      allStates.map(state => {
        const copy = { ...category };
        copy.state = state;
        return copy;
      })
    );
  });
  return result;
};

const getCurrentStage = async () => {
  const { Category, Review } = db;
  const opt = { attributes: ['id'] };
  const anyReview = await Review.findOne(opt);
  if (anyReview) {
    return STAGE_THREE;
  }
  const anyCategory = await Category.findOne(opt);
  if (anyCategory) {
    return STAGE_TWO;
  }
  return STAGE_ONE;
};

const stageOne = async () => {
  const categories = await new ParserCategories().run();
  const categoriesWithStates = getCategoriesWithStates(categories);
  const { Category } = db;
  await Category.bulkCreate(categoriesWithStates);
};

const STATES_COUNT = 59;

const stageTwo = async () => {
  let categories;
  let offset = 0;
  const limit = STATES_COUNT;
  const { Category } = db;
  const promises = [];
  do {
    // eslint-disable-next-line no-await-in-loop
    categories = await Category.findAll({
      offset,
      limit,
      order: [['id']],
      attributes: { exclude: ['id'] },
    });
    const promise = pool.exec('processCategories', [categories]);
    promises.push(promise);
    offset += limit;
  } while (categories && categories.length);
  await Promise.all(promises);
};

const stageThree = async () => {
  let categories;
  let offset = 0;
  const limit = 1;
  const { Category } = db;
  const promises = [];
  do {
    // eslint-disable-next-line no-await-in-loop
    categories = await Category.findAll({
      offset,
      limit,
      order: [['id']],
    });
    const promise = pool.exec('processReviews', [categories]);
    promises.push(promise);
    offset += limit;
  } while (categories && categories.length);
  await Promise.all(promises);
};

const main = async () => {
  await IO.Init();
  const stage = await getCurrentStage();
  switch (stage) {
    case STAGE_ONE:
      await stageOne();
    // eslint-disable-next-line no-fallthrough
    case STAGE_TWO:
      await stageTwo();
    // eslint-disable-next-line no-fallthrough
    case STAGE_THREE:
    // await stageThree();
    // eslint-disable-next-line no-fallthrough
    default:
  }
  return pool.terminate();
};

main().then(() => {
  logger.info('Script is finished.');
});
