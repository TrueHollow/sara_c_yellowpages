const cheerio = require('cheerio');
const Net = require('./Net');
const logger = require('../logger')('service/ParserCategories.js');

const URL_EXAMPLE = 'https://www.yellowpages.com/fresno-ca';

class ParserCategories {
  parse(html) {
    const $ = cheerio.load(html);
    const aTags = $('.popular-cats a');
    if (!aTags || aTags.length === 0) {
      throw new Error('Markup is changed!');
    }
    const result = [];
    aTags.each((i, aTag) => {
      const { href } = aTag.attribs;
      if (href) {
        const name = $(aTag)
          .text()
          .trim();
        result.push({
          name,
          href,
        });
      } else {
        logger.warn('Unexpected aTag (%s)', JSON.stringify(aTag));
      }
    });
    logger.info('Found (%d) popular categories.', result.length);
    return result;
  }

  async run() {
    logger.info('Fetching popular categories');
    const html = await Net.GetHtmlPage(URL_EXAMPLE);
    return this.parse(html);
  }
}

module.exports = ParserCategories;
