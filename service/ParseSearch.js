const cheerio = require('cheerio');
const Net = require('./Net');
const logger = require('../logger')('service/ParseSearch.js');

const URL_ROOT = 'https://www.yellowpages.com';
const URL_TEMPLATE = `${URL_ROOT}/search`;

class ParseSearch {
  static getNextPageUrl($) {
    const aTag = $('.scrollable-pane .pagination .next').get(0);
    if (!aTag) {
      return null;
    }
    const { href } = aTag.attribs;
    if (!href) {
      logger.warn('No href on next link, strange.');
    }
    return `${URL_ROOT}${href}`;
  }

  static getAdsLinks($) {
    const result = [];
    const aTags = $('.search-results.organic .business-name');
    aTags.each((i, aTag) => {
      const { href } = aTag.attribs;
      if (!href) {
        logger.warn('No href on ads, strange.');
      } else {
        result.push(`${URL_ROOT}${href}`);
      }
    });
    return result;
  }

  async run(category) {
    const { name, state } = category;
    let url = encodeURI(
      `${URL_TEMPLATE}?search_terms=${name}&geo_location_terms=${state}`
    );
    let data = [];
    do {
      // eslint-disable-next-line no-await-in-loop
      const html = await Net.GetHtmlPage(url);
      const $ = cheerio.load(html);
      url = ParseSearch.getNextPageUrl($);
      const urls = ParseSearch.getAdsLinks($);
      data = data.concat(
        urls.map(link => {
          const cleanUrl = link.split('?')[0];
          return {
            url: cleanUrl,
            category: name,
            state,
          };
        })
      );
    } while (url);
    return data;
  }
}

module.exports = ParseSearch;
