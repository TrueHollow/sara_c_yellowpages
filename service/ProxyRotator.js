const fs = require('fs');
const path = require('path');

let position = -1;
let configCache;
let lastUpdateCache = new Date(0);
const CACHE_INTERVAL = 1000 * 60 * 15;
const CONFIG_FULLPATH = path.resolve(__dirname, '..', 'config', 'index.json');
const getConfigSync = () => {
  const str = fs.readFileSync(CONFIG_FULLPATH, { encoding: 'utf8' });
  configCache = JSON.parse(str);
  lastUpdateCache = new Date();
};

const getNewProxy = () => {
  const now = new Date();
  if (now - lastUpdateCache > CACHE_INTERVAL) {
    getConfigSync();
  }

  const { ProxyPool } = configCache;
  if (!Array.isArray(ProxyPool) || ProxyPool.length === 0) {
    return;
  }
  position += 1;
  if (position >= ProxyPool.length) {
    position = 0;
  }
  const proxy = ProxyPool[position];
  process.env.http_proxy = proxy;
  process.env.https_proxy = proxy;
};

module.exports = {
  getNewProxy,
};
