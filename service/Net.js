const request = require('request');
const UserAgent = require('user-agents');
const logger = require('../logger')('service/Net.js');
const { getNewProxy } = require('./ProxyRotator');

const HTTP_OK = 200;

request.defaults({ pool: { maxSockets: Infinity }, timeout: 3000 });

const userAgentOptions = { deviceCategory: 'desktop' };

const delay = async (timeout = 100) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
};

const GetRequest = async url => {
  return new Promise((resolve, reject) => {
    getNewProxy();
    request(
      {
        url,
        method: 'GET',
        headers: {
          'User-Agent': new UserAgent(userAgentOptions).toString(),
        },
        gzip: true,
      },
      (err, iM, html) => {
        if (err) {
          return reject(err);
        }
        if (iM.statusCode !== HTTP_OK) {
          return reject(new Error(`Response code: ${iM.statusCode}`));
        }
        return resolve(html);
      }
    );
  });
};

class Net {
  static async GetHtmlPage(url) {
    let html;
    do {
      try {
        logger.debug(`Performing request (${url})...`);
        // eslint-disable-next-line no-await-in-loop
        html = await GetRequest(url);
        logger.debug(`...request (${url}) is finished`);
      } catch (e) {
        logger.error('(%s): %s', url, e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restart request (%s)', url);
      }
    } while (!html);
    return html;
  }
}

module.exports = Net;
