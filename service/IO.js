const fs = require('fs');
const path = require('path');
const stringify = require('csv-stringify');
const db = require('../db');
const logger = require('../logger')('service/IO.js');

const OUTPUT_DIRECTORY = path.resolve(__dirname, '../output');

const createOutputDirectory = async () => {
  return new Promise((resolve, reject) => {
    fs.mkdir(`${OUTPUT_DIRECTORY}`, { recursive: true }, err => {
      if (err) {
        return reject(err);
      }
      logger.debug('Output directory was created.');
      return resolve();
    });
  });
};

function isIterable(obj) {
  // checks for null and undefined
  if (obj == null) {
    return false;
  }
  return typeof obj[Symbol.iterator] === 'function';
}

const getCSVString = async (category, arr) => {
  return new Promise((resolve, reject) => {
    const options = {
      delimiter: ',',
    };
    stringify(arr, options, (err, records) => {
      if (err) {
        return reject(err);
      }
      return resolve(records);
    });
  });
};

const writeToOutput = async (category, str) => {
  const { name, state } = category;
  const fullPath = path.resolve(OUTPUT_DIRECTORY, `${name}.${state}.csv`);
  return new Promise((resolve, reject) => {
    fs.writeFile(fullPath, str, { flag: 'a' }, err => {
      if (err) {
        return reject(err);
      }
      return resolve();
    });
  });
};

class IO {
  static async Init() {
    await createOutputDirectory();
    const { sequelize } = db;
    return sequelize.sync();
  }

  static async SaveResult(category, data) {
    if (!isIterable(data)) {
      throw new Error('Data must be iterable!');
    }
    logger.debug('Saving result to output file...');
    const str = await getCSVString(category, data);
    await writeToOutput(category, str);
    logger.debug('...result was saved.');
  }
}

module.exports = IO;
