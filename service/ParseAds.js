const cheerio = require('cheerio');
const Net = require('./Net');
const IO = require('./IO');
const logger = require('../logger')('service/ParseAds.js');

// https://www.yellowpages.com/search?search_terms=vegetarian+restaurants&geo_location_terms=Ca
const URL_ROOT = 'https://www.yellowpages.com';
const URL_TEMPLATE = `${URL_ROOT}/search`;

class ParseAds {
  static async ParseAdsPages(url) {
    const cleanUrl = url.split('?')[0];
    const html = await Net.GetHtmlPage(cleanUrl);
    const $ = cheerio.load(html);
    const name = $('.sales-info')
      .text()
      .trim();
    const address = $('.contact .address')
      .text()
      .trim();
    const phone = $('.contact .phone')
      .text()
      .trim();
    let rating = null;
    const ratingTag = $('.ratings .rating-stars').get(0);
    if (ratingTag) {
      const tag = $(ratingTag);
      if (tag.hasClass('half')) {
        rating += 0.5;
      }

      if (tag.hasClass('zero')) {
        rating += 0;
      } else if (tag.hasClass('one')) {
        rating += 1;
      } else if (tag.hasClass('two')) {
        rating += 2;
      } else if (tag.hasClass('three')) {
        rating += 3;
      } else if (tag.hasClass('four')) {
        rating += 4;
      } else if (tag.hasClass('five')) {
        rating += 5;
      } else {
        logger.warn('Unknown rating (%s)', JSON.stringify(ratingTag));
      }
    }

    let website = null;
    const aTag = $('.website-link').get(0);
    if (aTag) {
      website = aTag.attribs.href;
    }

    let email = null;
    const emailTag = $('.email-business').get(0);
    if (emailTag) {
      const emailHref = emailTag.attribs.href;
      if (emailHref) {
        email = emailHref.replace('mailto:', '');
      }
    }

    const categories = [];
    $('dd.categories a').each((i, tag) => {
      categories.push(
        $(tag)
          .text()
          .trim()
      );
    });

    const extraPhones = [];
    $('dd.extra-phones').each((i, tag) => {
      $('p', tag).each((j, pTag) => {
        extraPhones.push(
          $(pTag)
            .text()
            .trim()
        );
      });
    });

    const hours = [];
    $('.open-details tr').each((i, tag) => {
      hours.push(
        $(tag)
          .text()
          .trim()
      );
    });

    let logo = null;
    const logoTag = $('dd.logo img').get(0);
    if (logoTag) {
      const logoSrc = logoTag.attribs.src;
      if (logoSrc) {
        logo = logoSrc;
      }
    }

    const images = [];
    $('.collage img').each((i, tag) => {
      const { src } = tag.attribs;
      if (src) {
        images.push(src);
      }
    });

    return {
      url: cleanUrl,
      name,
      address,
      phone,
      rating,
      website,
      categories,
      email,
      extra_phones: extraPhones,
      hours,
      logo,
      images,
    };
  }

  async run(category) {
    const { name, state } = category;
    let url = encodeURI(
      `${URL_TEMPLATE}?search_terms=${name}&geo_location_terms=${state}`
    );
    do {
      // eslint-disable-next-line no-await-in-loop
      const html = await Net.GetHtmlPage(url);
      const $ = cheerio.load(html);
      url = ParseAds.getNextPageUrl($);
      const ads = ParseAds.getAdsLinks($);
      const promises = ads.map(adUrl => ParseAds.ParseAdsPages(adUrl));
      // eslint-disable-next-line no-await-in-loop
      const adsDataArray = await Promise.all(promises);
      // eslint-disable-next-line no-await-in-loop
      await IO.SaveResult(category, adsDataArray);
    } while (url);
  }
}

module.exports = ParseAds;
