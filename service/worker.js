const workerpool = require('workerpool');
const genericPool = require('generic-pool');
const config = require('../config');
const ParseAds = require('./ParseAds');
const ParseSearch = require('./ParseSearch');
const db = require('../db');
const IO = require('./IO');
const logger = require('../logger')('service/worker.js');

const factoryParserSearch = {
  create() {
    return new ParseSearch();
  },
  destroy() {},
};

const { opts } = config.Parser.CategoriesPool;

const processCategories = async filteredCategories => {
  logger.info(`processCategories started (${process.pid})`);
  const { Review } = db;
  const myPool = genericPool.createPool(factoryParserSearch, opts);
  await Promise.all(
    filteredCategories.map(async category => {
      const parseAds = await myPool.acquire();
      const data = await parseAds.run(category);
      await myPool.release(parseAds);
      await Review.bulkCreate(data);
    })
  );

  return myPool
    .drain()
    .then(() => myPool.clear())
    .then(() => logger.info(`processCategories ended (${process.pid})`));
};

const processReviews = async category => {};

// create a worker and register public functions
workerpool.worker({
  processCategories,
  processReviews,
});
