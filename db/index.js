const Sequelize = require('sequelize');
const config = require('../config');
const logger = require('../logger')('db/index.js');

const configDB = config.database;
if (configDB.logging === true) {
  configDB.logging = message => {
    logger.debug(message);
  };
}

const sequelize = new Sequelize(configDB);

const db = {
  sequelize,
};

db.Review = require('./models/Review')(db);
db.Category = require('./models/Category')(db);

module.exports = db;
