const Sequelize = require('sequelize');

module.exports = ({ sequelize }) => {
  return sequelize.define('categories', {
    name: { type: Sequelize.STRING },
    state: { type: Sequelize.STRING },
  });
};
