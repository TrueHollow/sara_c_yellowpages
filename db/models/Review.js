const Sequelize = require('sequelize');

module.exports = ({ sequelize }) => {
  return sequelize.define('reviews', {
    url: { type: Sequelize.STRING },
    category: { type: Sequelize.STRING },
    state: { type: Sequelize.STRING },
  });
};
